var $body = $(document.body);

(function ($) {
    if ($body.hasClass('page--index')) {
        $('#countdown__el').countdown({
            date: '12/13/2017 16:00:00',
            offset: +1,
            day: 'Jour',
            days: 'Jours',
            hour: 'Heure',
            hours: 'Heures',
            minute: 'Minute',
            minutes: 'Minutes',
            second: 'Seconde',
            seconds: 'Secondes'
        }, function () {
            var win = window.open('https://vieillescharrues.asso.fr/2018/billetterie', '_blank');
            if (win) {
                win.focus();
            } else {
                console.log('Browser has blocked the popup!');
            }
        });
    }
})(jQuery);